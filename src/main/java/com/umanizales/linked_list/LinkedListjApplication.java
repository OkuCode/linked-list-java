package com.umanizales.linked_list;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinkedListjApplication {

    public static void main(String[] args) {
        SpringApplication.run(LinkedListjApplication.class, args);
    }

}
