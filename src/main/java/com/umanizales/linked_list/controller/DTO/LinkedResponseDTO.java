package com.umanizales.linked_list.controller.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class LinkedResponseDTO {
    private String message;
    private Object data;
    private List<ErrorDTO> errors;
}
