package com.umanizales.linked_list.controller;

import com.umanizales.linked_list.controller.DTO.LinkedResponseDTO;
import com.umanizales.linked_list.model.Child;
import com.umanizales.linked_list.service.LinkedListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "child")

public class ChildController {

    //Inject the service
    @Autowired
    private LinkedListService linkedListService;

    // Controller to add child
    @PostMapping
    public ResponseEntity<LinkedResponseDTO> addChild(@RequestBody Child child){
        return linkedListService.addChild(child);
    }

    //Controller to add to start
    @PostMapping(path = "addtostart")
    public ResponseEntity<LinkedResponseDTO> addChildToStart(@RequestBody Child child){
        return linkedListService.addChildToBegin(child);
    }

    @PostMapping(path = "addchildren")
    public ResponseEntity<LinkedResponseDTO> addChildren(@RequestBody List<Child> children){
        for (Child child:children){
            linkedListService.addChild(child);
        }

        return new ResponseEntity<>(new LinkedResponseDTO(
                "Success", linkedListService.listChildren(), null), HttpStatus.OK);
    }

    // Get data from a list
    @GetMapping
    public ResponseEntity<LinkedResponseDTO> listChildren(){
        return linkedListService.listChildren();
    }

    @GetMapping(path = "invert")
    public ResponseEntity<LinkedResponseDTO> invertChild(){
        return linkedListService.invertChildren();
    }

    //@GetMapping(path = "getcount")
    //public ResponseEntity<LinkedResponseDTO> getTotal(){
    //    return linkedListService.getTotal();
    //}

    @GetMapping(path = "count")
    public ResponseEntity<LinkedResponseDTO>count(){
        return linkedListService.count();
    }


    @GetMapping(path = "alone")
    public ResponseEntity<LinkedResponseDTO> listChildrenAlone(){
        return linkedListService.listChildrenAlone();
    }

    @GetMapping(path = "position")
    public ResponseEntity<LinkedResponseDTO> changePositions(){
        return linkedListService.changePositions();
    }
}
