package com.umanizales.linked_list.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LinkedList {
    public Node head;
    public int count;

    // Method addChild
    public void addChild(Child child){
       if (head == null) {
           head = new Node(child);
       } else {
           Node temp = this.head;

           while (temp.getNext() != null){
               temp = temp.getNext();
           }
           temp.setNext(new Node(child));
       }

       count += 1;
    }

    // Method addToBegin
    public void addToBegin(Child child){
       if(this.head == null){
           this.head = new Node(child);
       } else {
           Node temp = new Node(child);
           temp.setNext(this.head);
           this.head = temp;
       }

       count += 1;
    }

    // Invert Linked List
    public void invertChild(){
        if (this.head != null) {
            LinkedList listTemp = new LinkedList();
            Node temp = this.head;

            while (temp != null){
               listTemp.addToBegin(temp.getData());
               temp = temp.getNext();
            }

            this.head = listTemp.getHead();

        }
    }

    // Count nodes
    public int count(){
        int cont = 0;

        if (this.head != null) {
            Node temp = this.head;

            while (temp != null){
                cont += 1;
                temp = temp.getNext();
            }
        }
        return cont;
    }

    //List children
    public List<Child> listChildren(){
        if(this.head != null){
            Node temp = this.head;
            List<Child> list = new ArrayList<>();

            while (temp != null){
                list.add(temp.getData());
                temp = temp.getNext();
            }

            return list;
        }

        return null;
    }

    // Change positions
    public void changePositions(){
        if(this.head != null && this.head.getNext() != null){
           Child start  = this.head.getData();
           Node temp = this.head;
           while (temp.getNext() != null){
              temp = temp.getNext();
           }

           this.head.setData(temp.getData());
           temp.setData(start);
        }
    }
}
