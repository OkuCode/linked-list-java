package com.umanizales.linked_list.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Child {
    private String identification;
    private String name;
    private byte age;
    private String gender;
}
