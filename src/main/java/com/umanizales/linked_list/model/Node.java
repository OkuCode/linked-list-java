package com.umanizales.linked_list.model;

import lombok.Data;

@Data
public class Node {
    private Child data;
    private Node next;

    //Constructor for data
    public Node(Child data){
        this.data = data;
    }
}
