package com.umanizales.linked_list.service;

import com.umanizales.linked_list.controller.DTO.LinkedResponseDTO;
import com.umanizales.linked_list.model.Child;
import com.umanizales.linked_list.model.LinkedList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class LinkedListService {
    private LinkedList listChildren;

    public LinkedListService(){
        listChildren = new LinkedList();
    }

    public ResponseEntity<LinkedResponseDTO> addChild(Child boy){
       listChildren.addChild(boy);
       return new ResponseEntity<>(new LinkedResponseDTO(
               "Success", true, null), HttpStatus.OK
       );
    }
    public ResponseEntity<LinkedResponseDTO> addChildToBegin(Child boy){
        listChildren.addChild(boy);
        return new ResponseEntity<>(new LinkedResponseDTO(
                "Success", true, null), HttpStatus.OK
        );
    }

    public ResponseEntity<LinkedResponseDTO> listChildren(){
        return new ResponseEntity<>(new LinkedResponseDTO(
                "Success", listChildren.getHead(), null), HttpStatus.OK
        );
    }

    public ResponseEntity<LinkedResponseDTO> listChildrenAlone(){
        return new ResponseEntity<>(new LinkedResponseDTO(
                "Success", listChildren.listChildren(), null), HttpStatus.OK
        );
    }
    public ResponseEntity<LinkedResponseDTO> invertChildren(){
        listChildren.invertChild();
        return new ResponseEntity<>(new LinkedResponseDTO(
                "Success", listChildren.getHead(), null), HttpStatus.OK
        );
    }

    //public ResponseEntity<LinkedResponseDTO> getTotal(){
    //    return new ResponseEntity<>(new LinkedResponseDTO(
    //            "Success", listChildren.getTotal(), null), HttpStatus.OK);
    //////////////}

    public ResponseEntity<LinkedResponseDTO> count(){
        return new ResponseEntity<>(new LinkedResponseDTO(
                "Success", listChildren.count(), null), HttpStatus.OK);
    }

    public ResponseEntity<LinkedResponseDTO> changePositions(){
        listChildren.changePositions();
        return new ResponseEntity<>(new LinkedResponseDTO(
                "Success", true, null), HttpStatus.OK
        );
    }

}
